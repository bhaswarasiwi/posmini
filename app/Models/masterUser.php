<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class masterUser extends Model
{
    protected $table = "masteruser";
    protected $guarded = [];
}
