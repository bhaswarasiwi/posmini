<?php

namespace App\Http\Controllers;


use App\Models\masterUser;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    public function login()
    {
        return view('login');
    }

    public function checklogin(Request $request)
    {
        $check_data = masterUser::where([
            'emailUser' => $request->email,
            'passUser' => md5($request->password)
        ])->first();



        if($check_data)
        {
            Session_start();
            Session::flush();
            Session::put(['userData' => [
                'email' => $check_data->emailUser,
                'id' => $check_data->idUser,
                'level' => $check_data->levelUser,
                'name' => $check_data->namaUser
            ]]);

        return redirect('/admin');
        }
        else
        {
        return back()->with('error', 'Wrong Login Details');
        }
    }


    public function logout()
    {
            Session_start();
            Session::flush();
            return redirect(url('/'));
    }
}
