<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Crypt;
use App\Models\dataProduk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;

class produk extends Controller
{
    public function show()
    {
        $data['data']=dataProduk::all();
        return view('main.listproduk', $data);
    }

    public function index()
    {
        $data['data']=dataProduk::all();
        return view('produk.index', $data);
    }

    public function input()
    {
        return view('produk.input');
    }

    public function insert(Request $request)
    {
        $idUser = Session::get('userData')['id'];
        $time = Carbon::now();

        $arrayh = array();
        $gbr = $request->gambProduk;

        $gambProduk = time()."_1".$gbr->getClientOriginalName();
        $tujuan_upload = 'data_file/';
        $gbr->move($tujuan_upload,$gambProduk);


        $insertdata = dataProduk::updateOrCreate([
            'idProduk' => $request->idProduk,
            'idUser' => $idUser,
            'namaProduk' => $request->namaProduk,
            'hargProduk' => $request->hargProduk,
            'descProduk' => $request->descProduk,
            'katgProduk' => $request->katgProduk,
            'gambProduk' => $gambProduk,
            'created_by' => $request->created_by,
            'updated_at' => $time,
        ]);
        return redirect(url('/admin'));
    }

    public function edit($id)
    {
        $id = Crypt::decrypt($id);
        $data['data'] = dataProduk::where('idProduk',$id)->get();

        return view('produk.edit', $data);
    }

    public function update(Request $request)
    {
        $idUser = Session::get('userData')['id'];
        $time = Carbon::now();
        $id = $request->idProduk;

        $insertdata = dataProduk::where('idProduk',$id)
            ->update([
            'idUser' => $idUser,
            'namaProduk' => $request->namaProduk,
            'hargProduk' => $request->hargProduk,
            'descProduk' => $request->descProduk,
            'katgProduk' => $request->katgProduk,

            'created_by' => $request->created_by,
            'updated_at' => $time,
        ]);
        return redirect(url('/admin'));
    }

    public function delete($id)
    {
        $id = Crypt::decrypt($id);
        $data = dataProduk::where('idProduk',$id)->first();
	    File::delete('data_file/'.$data->gambProduk);

        // hapus data
        dataProduk::where('idProduk',$id)->delete();

        return redirect()->back();
    }


}
