<style type="text/css">
    @import url('https://fonts.googleapis.com/css2?family=Quicksand&display=swap');
    @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap');

    .box{
        max-width:600px;
        margin:0 auto;
        border:1px solid #ccc;
    }
    h3,h2,h1,p,td,th {
        font-family: 'Quicksand', sans-serif;
        color: rgb(65, 65, 65);
    }

    td{
        font-size: 12px;

    }
    th{
        font-size: 14px;
        padding: 5px;
    }

    body{
        background: linear-gradient(90deg, rgb(4, 181, 104) 0%, rgb(22, 185, 155) 27%, rgb(0, 255, 174) 100%);
    }
    .bgwhite{
        background: rgb(247,247,247);
    }

    .box{
        max-width: 300px;
        box-shadow: 2px 2px 30px rgba(0,0,0,0.2);
        border-radius: 10px;
        overflow: hidden;
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%,-50%);
    }

    .slide-img {
        max-height: 20%;
    }

    .slide-img img{
        max-width: 100%;
        object-fit: cover;
        box-sizing: border-box;
        background-color: rgb(255, 255, 255);
    }

    .detail-box{
        height: 250px;
        font-family: 'Quicksand', sans-serif;
        background: white;
    }

    .type{
        display: flex;
        flex-direction: column;
        text-align: center;
        padding: 5px;

    }

    .type a{
        color: #222222;
        margin: 5px 0px;
        font-weight: 700;
        letter-spacing: 0.5px;
        padding-right: 8px;
    }
    .type span{
        color: rgba(0, 0, 0, 0.5);
        font-family: 'Quicksand', sans-serif;
        font-size: 12px
    }
    .price{
        color: #333333;
        font-weight: 600;
        font-size: 1.1rem;
        font-family: 'Poppins', sans-serif;
        letter-spacing: 0.5px;
    }

    .tengah {
         margin: auto; max-width: 300px;
        }


</style>
