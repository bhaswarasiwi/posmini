<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="width=device-width">
    <title>Point Of Sales | mini</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/5.2.3/collection/components/icon/icon.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.39.0/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/icheck-bootstrap/3.0.1/icheck-bootstrap.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqvmap/1.5.1/jqvmap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/3.0.5/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/overlayscrollbars/1.13.0/css/OverlayScrollbars.min.css">
    {{-- bootstrap --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

    <link type="text/css" rel="stylesheet" href="css/lightslider.css" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/js/lightslider.min.js" integrity="sha512-Gfrxsz93rxFuB7KSYlln3wFqBaXUc1jtt3dGCp+2jTb563qYvnUBM/GP2ZUtRC27STN/zUamFtVFAIsRFoT6/w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css" integrity="sha512-yJHCxhu8pTR7P2UgXFrHvLMniOAL5ET1f5Cj+/dzl+JIlGTh5Cz+IeklcXzMavKvXP8vXqKMQyZjscjf3ZDfGA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    @include('../css.style')

</head>
<body>
    <nav class="navbar navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link"  href="#" role="button">
              <p>Majoo Teknologi Indonesia</p>
            </a>
          </li>
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
          <!-- Notifications Dropdown Menu -->
          <li class="nav-item dropdown"> </li>

          <li class="nav-item d-none d-sm-inline-block">
            <a href="{{ url('/admin') }}" class="nav-link">Admin</a>
          </li>
        </ul>
      </nav>

      <section >

        <div class="row" style="padding: 2%">
            @foreach($data as $p)
            <div class="col-md-3">
                <div class="card card-primary">
                    <div class="card-header" style="background-color: white; text-align: center;">
                        <img src="{{ url('/data_file/'.$p->gambProduk) }}" style="max-width: 300px; height: auto;" alt="">
                    </div>
                    <div class="card-body" style="text-align: center;">
                            <a href="#" style="font-size: 20px">{{$p->namaProduk}}</a>
                            <br>
                            <a href="#"><sup style="font-size: 10px;">Rp</sup>{{number_format($p->hargProduk)}} </a>
                            <br>
                            <br>
                        <p style="font-size: 14px;">{{$p->descProduk}}</p>
                    </div>
                    <div class="card-footer" style="background-color: white; text-align: center; padding:5px;">
                        <button class="btn btn-info" type="button">Beli</button>
                    </div>
                </div>
            </div>
            @endforeach
        </div>


      </section>

</body>

</html>
