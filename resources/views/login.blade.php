<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Login POS mini</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="preconnect" href="https://fonts.gstatic.com">

        @include('../css.style')
    </head>

    <body>

    <div style="padding: 5%;">
        <div class="container-fluid bgwhite" style=" border-radius: 10px; ">
        <h3 align="center">LOGIN</h3><br/>

        {{-- @if(isset(Auth::user()->email))
            <script>window.location="/main/helpdesk";</script>
        @endif --}}

        @if ($message = Session::get('error'))
        <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
        </div>
        @endif

        @if (count($errors) > 0)
            <div class="alert alert-danger">
            <ul>
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
            </ul>
            </div>
        @endif

        <form method="post" action="{{ url('/main/checklogin') }}">
            {{ csrf_field() }}
            <div class="form-group" >
            <p>Enter Email</p>
            <input type="email" name="email" class="form-control" />
            </div>
            <div class="form-group" >
            <p>Enter Password</p>
            <input type="password" name="password" class="form-control" />
            </div>
            <br>
            <div class="form-group" style="text-align: center;">
            <input type="submit" name="login" class="btn btn-success form-control" value="Login" />
            </div>
        </form>
        </div>
    </div>

    </body>
</html>
