<!-- Main Sidebar Container -->

<aside class="main-sidebar sidebar-dark-primary elevation-4" >

    {{-- modal add item --}}

      <!-- Brand Logo -->


      <!-- Sidebar -->
      <div class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <a href="{{url('/')}}" class="brand-link" style="text-align: center;">
                <span  style="color: white;">Point Of Sales Mini</span>
            </a>

          <div class="info" >
            <p href="#" class="d-block" style="color: white; text-align: center">Hallo {{Session::get('userData')['name']}}</p>
          </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
              <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

                <li class="nav-item has-treeview menu-close" >
                    <div style="background-color:rgb(241, 255, 253);  border-radius: 8px;">
                        <a href="#" class="nav-link ">
                        <i class="nav-icon fas fa-cubes"></i>
                        <p >
                            Produk
                            <i class="fas fa-angle-left right"></i>
                        </p>
                        </a>
                    </div>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('/main/paketlist') }}" class="nav-link">
                            <i></i>
                            <p style="color: white">Produk List</p>
                            </a>
                        </li>
                    </ul>
            </nav>
            <!-- /.sidebar-menu -->
          </div>

        </aside>
