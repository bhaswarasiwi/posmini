
    @extends('master')
    @section('konten')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

    <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <h2>Produk</h2>

                <div class="row">
                    <div class="col-12">
                      <div class="card">
                        <div class="card-header">
                        <a href="{{ url('/input')}}" class="btn btn-primary">Add Data</a>

                        </div>
                        <div class="card-header">
                          <h3 class="card-title"></h3>
                          {{-- Search --}}
                          <div class="card-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                              <input type="text" id="myInput" onkeyup="myFunction()" class="form-control float-right" placeholder="Search">
                            </div>
                          </div>
                        </div>

                <div class="card-body table-responsive p-0" style="height: 300px;">
                    <table id="myTable" class="sortable tableFixHead table-striped table-bordered" style="width:100%">
                        <thead align="center">
                          <tr class="header">
                            <th>Id Produk</th>
                            <th>Nama</th>
                            <th>Kategory</th>
                            <th>Harga</th>
                            <th>Deskripsi</th>
                            <th>Gambar</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $p)
                        <tr class="item">
                          <td align="center">{{$p->idProduk}}</td>
                          <td >{{$p->namaProduk}} </td>
                          <td >{{$p->katgProduk}} </td>
                          <td >{{$p->hargProduk}} </td>
                          <td >{{$p->descProduk}} </td>
                          <td ><img width="100px" onclick="onClick(this)" src="{{ url('/data_file/'.$p->gambProduk) }}" alt=""></td>
                          <td align="center" style="padding: 10px">
                            <a href="{{url('/')}}/edit/{{ encrypt($p->idProduk)}}" class="btn btn-sm btn-warning">Edit</a>
                            <a onclick="if(confirm('Want to delete?')){}else{return false}" href="{{url('/')}}/delete/{{ encrypt($p->idProduk) }} " class="btn btn-sm btn-danger">Delete</a>
                          </td>

                        </tr>
                        @endforeach
                      </tbody>
                </table>
            </div>
            </div>
            </div>
            </div>
            </div>
        </div>

    </div>

    @include('../layout.javascript')

</body>

</html>
@endsection
