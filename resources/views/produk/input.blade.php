@extends('master')
@section('konten')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<br>
<div class="content">
<div class="card card-primary">
<div class="card-header">
    <h4>Formulir Tambah Produk</h4>
</div>
<div class="card-body">
    <form method="post" action="{{ url('/insert')}}" enctype="multipart/form-data">
        {{ csrf_field() }}

        <div class="form-group">
            <input type="hidden" name="created_by" value="{{Session::get('userData')['name']}}" class="form-control">
        </div>

          <div class="form-group">
            <label for="">Nama Produk</label>
            <input type="text" name="namaProduk" class="form-control shadow-sm">
          </div>

          <div class="form-group">
            <label for="">Kategori Produk</label>
            <select class="form-control shadow-sm" name="katgProduk">
                <option value="Basic">Basic</option>
                <option value="Advance">Advance</option>
                <option value="Super">Super</option>
            </select>
          </div>

          <div class="form-group">
            <label for="">Harga Produk</label>
            <input type="number" name="hargProduk" class="form-control shadow-sm">
          </div>

          <div class="form-group">
            <label for="">Descripsi Produk</label>
            <textarea name="descProduk" id="" class="form-control shadow-sm"></textarea>
          </div>

          <div class="form-group">
            <label for="">Upload Gambar</label>
            <input type="file" name="gambProduk" class="form-control shadow-sm">
          </div>

          <br>
          <div style="text-align: center">
          <button type="submit" class="btn btn-lg btn-success"  >Submit</button>
          </div>
          <br>

    </form>
          <!-- /.card-body -->
    </div>
</div>
</div>
</div>



<!-- jQuery -->
@include('../layout.javascript')
<!-- Bisa di tambahkan lagi jquery langsung disini jika di butuhkan -->
</body>

</html>
@endsection
