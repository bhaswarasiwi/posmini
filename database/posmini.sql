-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 18, 2021 at 06:17 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `posmini`
--

-- --------------------------------------------------------

--
-- Table structure for table `masteruser`
--

CREATE TABLE `masteruser` (
  `idUser` int(11) NOT NULL,
  `namaUser` varchar(16) NOT NULL,
  `emailUser` varchar(30) NOT NULL,
  `passUser` varchar(32) NOT NULL,
  `levelUser` char(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` varchar(16) NOT NULL,
  `updated_at` varchar(22) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `masteruser`
--

INSERT INTO `masteruser` (`idUser`, `namaUser`, `emailUser`, `passUser`, `levelUser`, `created_at`, `created_by`, `updated_at`) VALUES
(1, 'adminPOS', 'admin@pos.mini', '69adeecb16e4391f6d67c8bc997e6cd3', '1', '2021-10-18 01:02:31', 'owner', 'none');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `idProduk` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `namaProduk` varchar(50) NOT NULL,
  `descProduk` varchar(100) NOT NULL,
  `hargProduk` int(11) NOT NULL,
  `katgProduk` varchar(20) NOT NULL,
  `gambProduk` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` varchar(16) NOT NULL,
  `updated_at` varchar(22) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`idProduk`, `idUser`, `namaProduk`, `descProduk`, `hargProduk`, `katgProduk`, `gambProduk`, `created_at`, `created_by`, `updated_at`) VALUES
(5, 1, 'majoo El', 'awdawdawdwdaw', 11231231, 'Basic', '1634571006_1paket-lifestyle.png', '2021-10-18 15:30:20', 'adminPOS', '2021-10-18 15:30:19');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `masteruser`
--
ALTER TABLE `masteruser`
  ADD PRIMARY KEY (`idUser`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`idProduk`),
  ADD KEY `produk` (`idUser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `masteruser`
--
ALTER TABLE `masteruser`
  MODIFY `idUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `idProduk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `produk`
--
ALTER TABLE `produk`
  ADD CONSTRAINT `produk` FOREIGN KEY (`idUser`) REFERENCES `masteruser` (`idUser`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
