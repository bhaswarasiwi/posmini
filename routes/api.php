<?php

use App\Http\Controllers\apiProduk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app('Dingo\Api\Routing\Router');

$api->version(['v1', 'v2'], function($api) {

    $api->get('test', function() {
        return "Test API, ";
    });
});

// Route::get ('/produk/data/', [apiProduk::class,'index']);
