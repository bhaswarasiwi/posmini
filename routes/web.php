<?php

use App\Http\Controllers\Controller;
use App\Http\Controllers\produk;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get ('/', [produk::class,'show']);
Route::get ('/login', [Controller::class,'login']);
Route::get ('/logout', [Controller::class,'logout']);
Route::post('/main/checklogin', [Controller::class,'checklogin']);

Route::get ('/produk/data', [produk::class,'data']);

Route::middleware(['cekLogin'])->group(function () {
    Route::get ('/admin', [produk::class,'index']);
    Route::get ('/input', [produk::class,'input']);
    Route::get ('/edit/{id}', [produk::class,'edit']);
    Route::post ('/insert', [produk::class,'insert']);
    Route::post ('/update', [produk::class,'update']);
    Route::get ('/delete/{id}', [produk::class,'delete']);
});
